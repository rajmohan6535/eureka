import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './components/App';
import routes from './routes/routes';
import store from './Services/store';

/**
 * @use Global vue components
 */
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
/**
 * @Global Axios global config
 * @type {{"X-Requested-With": string}}
 */
axios.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'};
window.axios = axios;

/**
 * @Init Initializing vue router view
 * @type {VueRouter}
 */
const router = new VueRouter({routes: routes, hashbang: true, linkExactActiveClass: 'active'});

new Vue(Vue.util.extend({router, store}, App)).$mount('#app');
