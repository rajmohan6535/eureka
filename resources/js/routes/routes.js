import Home from "../components/pages/Home";
import Test from "../components/pages/Test";

export default [
    {
        name: 'Home',
        path: '/',
        component: Home,
    },
    {
        name: 'Test',
        path: '/test',
        component: Test,
    },
];
